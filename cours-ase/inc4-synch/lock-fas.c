void verrouiller (int *verrou) {
  while (fetch_and_set (verrou) != 0)
    ;       // attente active
}
void deverrouiller (int *verrou) {
  *verrou = 0 ;
}
