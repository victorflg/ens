void lasuite (void)
{
    int *code = 102 ;		// adresse du code stocké par le matériel lors de l'interruption ou l'exception

    switch (*code) {
	case 0 : traiter_interruption () ; break ;
	case 1 : traiter_div_par_zero () ; break ;
	case 2 : traiter_instr_invalide () ; break ;
	case 3 : traiter_instr_privilegiee () ; break ;
	case 4 : traiter_trap () ; break ;
	default : panic ("exception invalide") ; break ;	// toujours prévoir l'impossible
    }
}
