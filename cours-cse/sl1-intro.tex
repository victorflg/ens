\def\inc{inc1-intro}

\newcommand {\critere} [1] { %
    % boîte de taille fixe
    \makebox [.4\linewidth] [l] {#1 :}
    % vert foncé bien aligné grâce à la boîte
    \textcolor [HTML] {007700} {\textbf{oui}} %
}


\titreA {Introduction}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Organisation de l'UE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Organisation de l'UE}

\begin {frame} {Objectifs}
    Objectifs de l'UE :

    \begin {itemize}
	\item comprendre l'implémentation d'un système d'exploitation
	    \begin {itemize}
		\item aller au delà d'une vision superficielle
		\item se confronter à la réalité d'un système réel
	    \end {itemize}

	\item compréhension « en creux » des problématiques
	    \begin {itemize}
		\item des systèmes temps-réel
		\item des systèmes embarqués
		\item de la virtualisation
		\item de l'administration système
	    \end {itemize}

    \end {itemize}

    Moyen :

    \begin {itemize}
	\item Étude et modification d'un \textbf {système réel}
    \end {itemize}

    Prérequis :

    \begin {itemize}
	\item Langage C
	\item API des primitives système (POSIX)
	\item Architectures matérielles
    \end {itemize}

\end {frame}

\begin {frame} {Organisation}
    \begin {itemize}
	\item cours magistral : 7 séances

	    \begin {itemize}
		\item concepts
		\item mécanismes matériels
	    \end {itemize}

	\item travaux dirigés : 6 séances

	    \begin {itemize}
		\item compréhension du code source
		\item proposition de modification
	    \end {itemize}

	\item travaux pratiques : 3 séances

	    \begin {itemize}
		\item initiation aux outils
		\item TP noté
	    \end {itemize}

	\item contrôle final

    \end {itemize}
\end {frame}

\begin {frame} {Bibliographie}

    \begin {itemize}
	\fC
	\item R.H. Arpaci-Dusseau, A.C. Arpaci-Dusseau 
	    « Operating Systems: Three Easy Pieces »,
	    Arpaci-Dusseau Books (2015)
	    \url{http://pages.cs.wisc.edu/~remzi/OSTEP/}

	\item A. Silberschatz, P.B. Galvin, G. Gagne,
	    « Operating System Concepts » 9th Ed, Wiley (2013)

	\item M.J. Bach, « The Design of the Unix Operating
	    System », Prentice/Hall (1986)

	\item M.K. McKusick, G.V. Neville-Neil, R.N.M. Watson,
	    « The Design and Implementation of the FreeBSD Operating
	    System », Addison-Wesley (2015)

	\item J. Lions, « A Commentary on the Sixth Edition Unix
	    Operating System » (1976)
	    \\
	    \url{http://www.lemis.com/grog/Documentation/Lions/}
	    \\
	    (réimprimé en 1996 par Peer-To-Peer Communications)

%	\item « Intel 386 Programmer’s Reference Manual » (1986)
%	    \url{http://css.csail.mit.edu/6.858/2015/readings/i386.pdf}

	\item « Intel 486 Microprocessor Family Programmer’s
	    Reference Manual » (1990)
	    \url{https://en.wikichip.org/w/images/7/79/i486_MICROPROCESSOR_PROGRAMMER'S_REFERENCE_MANUAL_(1990).pdf}

%	\item « Intel 64 and IA-32 Architectures Software Developer’s Manual »
%	    {\fE 
%	    \url{https://software.intel.com/en-us/download/intel-64-and-ia-32-architectures-sdm-combined-volumes-1-2a-2b-2c-2d-3a-3b-3c-3d-and-4}
%	    }

    \end {itemize}

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rappels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Rappels : le noyau}

\begin {frame} {Objectifs}
    Définition :

    \begin {quote}
	Le noyau est constitué de l'ensemble minimum des primitives
	système nécessaires pour atteindre les deux objectifs
	fondamentaux :

	\begin {itemize}
	    \item partager équitablement les ressources
	    \item garantir la sécurité des données
	\end {itemize}

    \end {quote}
\end {frame}

\begin {frame} {Support matériel}
    Support matériel indispensable pour assumer le rôle de gardien
    des ressources :

    \begin {enumerate}
	\item processeur avec (minimum) deux modes d'exécution

	    \begin {itemize}
		\item l'accès direct aux ressources est réservé au seul
		    noyau
		\item sinon : n'importe quel programme peut accéder
		    aux ressources sans contrôle
	    \end {itemize}

	\item mécanisme d'interruptions matérielles

	    \begin {itemize}
		\item permet de bénéficier du parallélisme des
		    périphériques
		\item sinon : noyau doit surveiller l'achèvement
		    des requêtes

	    \end {itemize}

	\item horloge capable d'interrompre périodiquement le processeur

	    \begin {itemize}
		\item permet au noyau de reprendre le contrôle
		\item sinon : système non préemptif
	    \end {itemize}

	\item mécanisme de traduction/protection mémoire

	    \begin {itemize}
		\item assure l'indépendance des espaces mémoire
		\item sinon : pas de protection des processus les uns par
		    rapport aux autres
	    \end {itemize}

    \end {enumerate}

\end {frame}

\begin {frame} {Support matériel -- Modes d'exécution}
    Mode d'exécution caractérisé par un bit dans le registre d'état (SR)
    du processeur
    \begin {itemize}
	\item mode non privilégié :
	    \begin {itemize}
		\item mode d'exécution pour le code utilisateur
		\item certaines instructions sont interdites
		\item tentative d'exécution d'une instruction privilégiée
		    \implique
		    exception (interruption interne au processeur)
		\item même les programmes de l'administrateur sont
		    exécutés en mode non privilégié (code utilisateur)
	    \end {itemize}
	\item mode privilégié :
	    \begin {itemize}
		\item mode d'exécution pour le noyau
		\item accès à toutes les instructions du processeur
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Support matériel -- Modes d'exécution}
    Exemples d'instructions privilégiées (à adapter en fonction du
    type de processeur) :

    \ctableau {\fC} {|l|l|} {
	IN, OUT & instructions d'E/S : accès direct aux périphériques \\
	écriture dans SR & pourrait modifier le mode d'exécution courant \\
	RESET & réinitialise le processeur \\
	RTI & retour d'interruption \\
    }
\end {frame}


\begin {frame} {Le noyau}
    Le noyau est chargé en mémoire au démarrage de l'ordinateur
    \begin {itemize}
	\item seul programme à utiliser le mode « privilégié »
	\item initialise le processeur, la mémoire, les périphériques
	\item « répond » aux requêtes des processus \\
	    \implique le noyau implémente les primitives système
    \end {itemize}

    \vspace* {3mm}
    Le noyau est un programme autonome :
    \begin {itemize}
	\item pas d'utilisation de bibliothèques externes
	\item édition de liens \implique un binaire unique et autonome
	\item ajout dynamique de modules
	    \begin {itemize}
		\item ajout de nouvelles fonctions
		    \begin {itemize}
			\item pilotes, systèmes de fichiers, etc.
			\item mise à jour de tables (ex: table des pilotes)
		    \end {itemize}
		\item édition de liens complémentaire au chargement
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Le noyau}
    Trois « accès » possibles au noyau :

    \vspace* {2mm}

    \begin {minipage} [c] {.35\linewidth}
	\includegraphics [width=1\linewidth] {\inc/acces}
    \end {minipage}
    \hfill
    \begin {minipage} [c] {.64\linewidth}
	% \fC
	\begin {itemize}
	    \item interruption matérielle
		\begin {itemize}
		    \item générée par un périphérique \\
			exemple : fin d'entrée/sortie
		    \item générée par l'horloge \\
			exemple : fin de quantum
		\end {itemize}
	    \item exception provoquée par le processus
		\begin {itemize}
		    \item violation de segment
		    \item division par 0
		    \item etc.
		\end {itemize}
	    \item appel d'une primitive système
		\begin {itemize}
		    \item instruction spéciale du processeur
		    \item considéré comme une exception
		\end {itemize}
	\end {itemize}
    \end {minipage}

    \vspace* {3mm}

    Traitement similaire des 3 types d'accès
\end {frame}

\begin {frame} {Appel des primitives système}
    En réalité, les primitives sont des fonctions de bibliothèque...
    \begin {center}
	\includegraphics [width=.9\textwidth] {\inc/trap-lib}
    \end {center}
\end {frame}

\begin {frame} {Appel des primitives système}
    Les appels à des primitives sont des fonctions de bibliothèque :
    \begin {itemize}
	\item compilateur : un appel à une primitive
	    système est traité comme un appel à n'importe quelle
	    fonction C

	    \begin {itemize}
		\item aucune spécificité des primitives système
	    \end {itemize}

	\item bibliothèque standard du C : ajout d'une fonction

	    \begin {itemize}
		\item codée en assembleur
		    \begin {itemize}
			\item squelette identique pour quasiment
			    toutes les primitives
		    \end {itemize}
		\item instruction spéciale du processeur (ex: \code{trap})
		\item retour en fonction de la retenue (bit « carry »)

		    \ctableau {\fD} {|c|c|} {
			\textbf{carry} & \textbf{registre r0 en retour}
			    \\
			0 & valeur de retour de la primitive \\
			1 & valeur à mettre dans \code{errno} \\
		    }
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Démarrage du noyau}
    Le noyau est chargé en mémoire au démarrage de l'ordinateur

    \vspace* {3mm}

    Plusieurs phases :
    \begin {enumerate}
	\addtocounter {enumi} {-1}
	\item interrupteur sur « on » (ou processeur réinitialisé) :
	    \begin {itemize}
		\item BIOS (ou équivalent, programme en ROM ou Flash)
		    démarre
		\item il initialise la mémoire et les périphériques
		\item il cherche les disques dont le secteur 0 contient
		    un numéro magique identifiant un secteur bootable
		    \implique choix
	    \end {itemize}
	\item le BIOS charge le secteur 0 du disque sélectionné
	    \begin {itemize}
		\item ce programme doit tenir dans un secteur \implique petit !
		\item c'est le chargeur primaire
		\item insuffisant pour lire le noyau (dépend du système
		    de fichiers) \implique besoin d'un chargeur secondaire
		\item ce programme charge un autre programme qui peut être
		    plus grand \implique emplacement conventionnel
	    \end {itemize}
    \end {enumerate}
\end {frame}

\begin {frame} {Démarrage du noyau}
    \begin {enumerate}
	\addtocounter {enumi} {1}
	\item le chargeur primaire charge le chargeur secondaire
	    \begin {itemize}
		\item ce programme est plus grand
		\item il est capable de lire le système de fichiers
		\item \implique capable de lire les blocs du noyau
		    éparpillés dans le secteur de fichiers
	    \end {itemize}

	\item le chargeur secondaire analyse le système de fichiers
	    pour lire les blocs du noyau
	    \begin {itemize}
		\item une fois le noyau chargé, le contrôle est
		    transféré \\
		    (PC $\leftarrow$ adresse du noyau en mémoire)
	    \end {itemize}
    \end {enumerate}

    Méthode alternative : mémoriser l'emplacement des blocs du
    noyau sur le disque pour éliminer le chargeur secondaire
\end {frame}

\begin {frame} {Démarrage du noyau}
    Le noyau est chargé en mémoire au démarrage de l'ordinateur
    \begin {center}
	\includegraphics [width=.9\textwidth] {\inc/boot}
    \end {center}
\end {frame}

\begin {frame} {Démarrage du noyau}
    Et après le chargement du noyau ?

    \begin {itemize}
	\item le noyau crée un descripteur de processus pour le pid 0
	    \begin {itemize}
		\item à partir de ce moment, le noyau exécute un processus
	    \end {itemize}
	\item ce processus (descripteur) est ensuite dupliqué \implique pid = 1
	\item le nouveau processus fait \code{execl ("/sbin/init", ...)}
	    \begin {itemize}
		\item version interne de \code{execl}
		\item chargement du code de \code{/sbin/init} en mémoire
		\item « retour » de primitive système \implique exécution
		    de \code{main}
		\item \code{/sbin/init} (ou \code{systemd}) exécute
		    de nouvelles primitives système
		    \begin {itemize}
			\item lancement des scripts de démarrage
			\item lancement des démons
		    \end {itemize}
	    \end {itemize}
	\item c'est parti...
    \end {itemize}
\end {frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Quel système étudier ?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Quel système étudier ?}

\begin {frame} {Quel système étudier ?}
    Quel support choisir pour étudier l'implémentation d'un système ?

    \medskip

    Critères possibles :
    \begin {itemize}
	\item sources accessibles \\
	    \implique étude de l'implémentation
	\item simple \\
	    \implique UE «~CSE~» (et pas Master «~CSE~»)
	\item utilisable \\
	    \implique mettre en pratique les concepts
	\item représentatif \\
	    \implique montrer des mécanismes universels
	\item bien écrit \\
	    \implique lire et modifier du code existant doit être facile
	\item documenté \\
	    \implique immersion facilitée
    \end {itemize}

\end {frame}

\begin {frame} {Évolution de la taille des noyaux}
    \vspace* {-2mm}
    \begin {center}
	\includegraphics [width=1\linewidth] {\inc/histsize}
    \end {center}
    \implique les systèmes actuels sont trop volumineux !
\end {frame}

\begin {frame} {Taille des noyaux}
    Pourquoi cette inflation entre 1970 et maintenant ?

    \begin {itemize}
	\item optimisations
	\item nouveaux besoins 
	    \begin {itemize}
		\item réseau
		\item sécurité
		\item ...
	    \end {itemize}
	\item besoins plus sophistiqués
	    \begin {itemize}
		\item fiabilité des signaux v7 \implique signaux POSIX
		\item systèmes de fichiers plus robustes
		\item plusieurs systèmes de priorité des processus \\
		    (round-robin, temps réel, etc.)
		\item ...
	    \end {itemize}
	\item réalité diverse
	    \begin {itemize}
		\item gammes de matériels étendues
		\item systèmes de fichiers multiples
		\item protocoles réseau multiples
		\item ...
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Taille des sous-systèmes}
    Illustration de cette inflation sur un système actuel
    \begin {center}
	\includegraphics [width=1\linewidth] {\inc/subsys}
    \end {center}
    {\fD Note : répartition entre sous-systèmes arbitraire (cf script
    de génération de ce graphique)}

\end {frame}

\begin {frame} {Langage de programmation}
    Langages de programmation des systèmes d'exploitation~:

    \begin {itemize}
	\item initialement : en assembleur
	\item tentatives en langage de haut niveau
	    \begin {itemize}
		\item Burroughs B5000 (1961) : dialecte d'Algol
		\item Multics (1968) : PL/1
	    \end {itemize}
	\item à partir de 1971 sur Unix : langage C
	\item depuis : C++, Rust, etc.

    \end {itemize}

    \medskip

    Même avec un langage de haut niveau, il faut un minimum d'assembleur
    pour l'interface de bas niveau avec le matériel.

\end {frame}

\begin {frame} {Langage de programmation}
    Proportion d'assembleur : illustration sur plusieurs systèmes
    \begin {center}
	\includegraphics [width=1\linewidth] {\inc/casm}
    \end {center}

    {\fD
    Note : « Source C » comprend les fichiers «~.c~», «~.h~»
    et «~Makefile~»}

\end {frame}

\begin {frame} {Quel système étudier ?}
    Alors, quel support choisir pour étudier l'implémentation d'un
    système d'exploitation ?

    \medskip

    Critères :
    \begin {itemize}
	\item sources accessibles
	\item simple
	\item utilisable
	\item représentatif
	\item bien écrit
	\item documenté
    \end {itemize}

\end {frame}

\begin {frame} {Retour dans le passé -- Lions Book (1976)}

    \begin {minipage} {.29\linewidth}
	\includegraphics [width=\linewidth] {\inc/bwk-lions}
	\\
	\creditphoto {Ben Lowe (Brian Kernighan lors d'une cérémonie
	aux Bell Labs en 2012)} {\ccby}
    \end {minipage}
    \begin {minipage} {.70\linewidth}
	\begin {itemize}
	    \item 1976 : John Lions, enseignant à l'University of New
		South Wales (Australie), rédige pour ses étudiants
		«~\textit{A Commentary on the Unix Operating System
		(with source code)}~»

	    \item source code = code propriété des Bell Labs \\
		\implique photocopies «~sous le manteau~»

	    \item ouvrage de référence \\
		\implique Ken Thompson : «~\textit{the best book on
		how an operating system works}~»

	    \item édité officiellement en 1996 \\
		(cf bibliographie)

	    \item toujours utilisé après plus de 40 ans !

	\end {itemize}
    \end {minipage}

\end {frame}

\begin {frame} {Aujourd'hui -- MIT 6.828}

    Cours « Operating System Engineering » (6.828) du MIT :

    \begin {itemize}
	\item 2002 : utilisation d'Unix Bell v6 et du
	    Lions Book

	    \begin {itemize}
		\item architecture matérielle : DEC PDP-11 (1975)
		    \\
		    \implique architecture obsolète, utilisation d'un
		    simulateur

		\item langage C : version de 1975
		    \\
		    \implique assez différent du C actuel

	    \end {itemize}

	\item 2006 : réécriture \implique xv6 

	    \begin {itemize}
		\item xv6 basé sur Unix Bell v6
		\item langage C actuel
		\item architecture Intel x86 : actuelle
		\item avec un livret d'accompagnement (basé sur le Lions Book)
		\item \url{https://pdos.csail.mit.edu/6.828/2018/xv6.html}
	    \end {itemize}

    \end {itemize}

    \bigskip

    {\fD Note~: en 2019, xv6 a basculé sur l'architecture Risc-V.}
\end {frame}

\begin {frame} {Quel système étudier ?}

    \textbf{Choix de xv6 pour ce cours}

    \medskip

    % Critères :
    \begin {itemize}
	\item \critere{sources accessibles} \\
	    {\fC\url{http://github.com/mit-pdos/xv6-public}}
	\item \critere{simple} \\
	    12$\thinspace$556 lignes de code
	\item \critere{utilisable} \\
	    {\fC\url{https://pdos.csail.mit.edu/6.828/2018/tools.html}}
	\item \critere{représentatif}
	\item \critere{bien écrit}
	\item \critere{documenté} \\
	    {\fC\url{https://pdos.csail.mit.edu/6.828/2018/xv6/xv6-rev10.pdf}}
	    \\
	    En français : {\fC\url{https://pdagog.gitlab.io/xv6-livret/xv6-livret.pdf}}
    \end {itemize}

\end {frame}

\begin {frame} {Quel système étudier ?}
    Xv6 n'est pas complet. Ses limitations sont :

    \begin {itemize}
	\item pas de mécanisme de swap \implique tout doit tenir en mémoire
	\item pas de notion d'utilisateur ou de groupe
	    \implique mono-utilisateur
	\item piles des processus limitées à une page mémoire
	\item pas de détection de fin de mémoire physique \\
	    \implique mémoire fixe = 240~Mo
	\item API des primitives très simplifiée \\
	    \implique pas d'\code{errno}, manque arguments (cf \code{exit}/\code{wait})
	\item etc.
    \end {itemize}

    \implique xv6 est uniquement à vocation «~pédagogique~»
    \\
    \implique les limitations sont autant de sujets d'exercices !

\end {frame}

\begin {frame} {Quel système étudier ?}
    En revanche, xv6 gère certains mécanismes plus élaborés
    qu'avec Unix Bell v6 :

    \begin {itemize}
	\item mémoire virtuelle : pagination du i386
	    \\
	    (Unix Bell v6 : non disponible sur le PDP-11)
	\item multi-processeurs : mécanismes de synchronisation avancés
	    \\
	    (Unix Bell v6 : non disponible sur le PDP-11)
	\item système de fichiers journalisé (simplifié)
	    \\
	    (Unix Bell v6 : système de fichiers simple)
    \end {itemize}

    \medskip

    \implique concepts actuels
\end {frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Xv6 en pratique
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Xv6 en pratique}

\begin {frame} {Xv6 en pratique}
    Sur un système Linux (non testé sur MacOS ou FreeBSD) :

    \begin {itemize}
	\item Récupération des sources de xv6

	    \begin {itemize}
		\item \url{https://github.com/mit-pdos/xv6-public}
		    \begin {itemize}
			\item attention : utiliser \code{git clone}
			    (et pas le lien de téléchargement)

			\item \code{git clone https://github.com/mit-pdos/xv6-public.git}
		    \end {itemize}
	    \end {itemize}

	\item Installation des paquets

	    \begin {itemize}
		\item make, gcc, gdb, qemu
	    \end {itemize}

	\item Compilation : «~\textit{just type make}~»

	    \begin {itemize}
		\item compile les sources du noyau et fait l'édition
		    de liens
		    \\
		    \implique fichier \code{kernel}

		\item compile le secteur de boot et fait l'édition
		    de liens
		    \\
		    \implique fichier \code{bootblock}
		\item crée l'image d'un disque et y installe
		    le secteur de boot et le noyau
		    \\
		    \implique fichier \code{xv6.img}
	    \end {itemize}
    \end {itemize}
\end {frame}


\begin {frame} {Xv6 en pratique}
    \begin {itemize}
	\item Test simple (sans débogueur) : \code {make qemu}

	    \begin {itemize}
		\item crée l'image \code{xv6.img} si ce n'est déjà fait
		\item compile les quelques utilitaires fournis avec xv6
		    \\
		    \implique fichiers \code{sh}, \code{ls}, \code{cat}, etc.
		\item crée une deuxième image disque et y installe
		    les utilitaires
		    \\
		    \implique fichier \code{fs.img}
		\item lance \code{qemu} avec les deux images disques
		    \\
		    \implique boot de xv6, lancement du shell xv6
	    \end {itemize}
    \end {itemize}

    \bigskip

    Test simple : inutile la plupart du temps \\
    \implique dans la pratique, il vaut mieux recourir au débogueur
\end {frame}


\begin {frame} {Xv6 en pratique}
    Pourquoi recourir à \code{qemu} ?

    \begin {itemize}
	\item rappel : \code{qemu} émule un PC sous x86 (ou d'autres
	    systèmes)

	\item on pourrait utiliser un vrai PC, mais...

	    \begin {itemize}
		\item erreur de programmation \implique crash du système
		\item en cas de crash, c'est tout le PC qui est planté
		    \implique \alert{on/off}...
		\item en cas de crash, on peut perdre tous les fichiers
		\item un vrai PC, c'est long à démarrer
		\item utiliser \code{qemu} permet d'utiliser \code{gdb}
		    pour interrompre le système, poser des points
		    d'arrêt, exécuter en mode «~pas à pas~», etc.
	    \end {itemize}

	\item bref, le développement noyau, c'est plus facile avec
	    un émulateur

	    \begin {itemize}
		\item les anciens n'avaient pas cette facilité...
	    \end {itemize}
    \end {itemize}

\end {frame}


\begin {frame} {Xv6 en pratique}
    \begin {itemize}
	\item Session de débogage (conseillé) :

	    \begin {itemize}
		\item \alert{attention} ! Ajouter au préalable la ligne
		    suivante dans  le fichier \code{\$HOME/.gdbinit}
		    (une fois pour toutes) :
		    \\
		    \code{add-auto-load-safe-path }....\code{/xv6-public/.gdbinit}

		    \medskip

		    Attention :
		    \begin {itemize}
			\item mettre le chemin complet du répertoire
			    \code{xv6-public}
			\item pas de \code{\~{}}, pas de \code{\$HOME}
		    \end {itemize}

		    \medskip

		\item \code{make qemu-nox-gdb}
		    \\
		    \implique compile, démarre la machine virtuelle, et
		    attend le débogueur

		    \medskip

		\item ouvrir une nouvelle fenêtre et lancer le débogueur
		    \\
		    \implique \code{gdb} \hspace* {5mm} (\textit{sans argument})

	    \end {itemize}
    \end {itemize}

    Astuce : démarrer \code{qemu} avec un seul
    processeur pour simplifier

\end {frame}


\begin {frame} {Commandes utiles de gdb}
    Petit mémo des commandes utiles de \code{gdb} :

    \ctableau {\fD} {|ll|} {
	\code{win} & scinde la fenêtre en deux (source en haut
	    / commandes gdb en bas)
	    \\
	\code{b mafct} & pose un point d'arrêt
	    à l'entrée de \code{mafct}
	    \\
	\code{c} & lance l'exécution
	    jusqu'au prochain point d'arrêt
	    \\
	\code{where} & affiche la pile
	    \\
	\code{p *toto[10]} & affiche l'élément
	    pointé par le 11e élément du
	    tableau \code{toto}
	    \\
	\code{p /x *toto[10]} & idem, mais affichage en hexadécimal
	    \\
	\code{s} & exécute une
	    instruction C
	    \\
	\code{n} & exécute une
	    instruction C sans s'arrêter si appel de fonction
	    \\
	\code{stepi} & exécute une instruction
	    assembleur
	    \\
	\code{info registers} & liste le contenu des registres
	    du processeur (abréviation : \code{i r})
	    \\
	\code{x/10wd toto} & affiche 10 mots (\textit{\textbf{w}ord\/},
	    32 bits) en \textbf{d}écimal à partir de l'adresse
	    \code{toto}
	    \\
	\code{k} & termine xv6
	    \\
	\code{q} & quitte le débogueur
	    \\

    }

\end {frame}
